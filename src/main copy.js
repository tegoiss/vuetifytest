import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router'
import cast from '../assets/cast.json';

const state = () => ({
  cast: [],
  sections: []
})

const mutations = {
  SET_CAST: (state, Array) => {
    state.cast = Array
    console.log('SET SEARCH', Array)
  }
}

const actions = {
  setCast: (state, Array) => {
    console.log('setCast', state, Array)
  }
}

const getters = {
  getCast: state => state.cast
}

Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')


