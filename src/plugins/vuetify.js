import Vue from 'vue';
import Vuetify from 'vuetify/lib';

import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light: {
                primary: '#3a676a',
                secondary: '#806582',
                accent: '#899a7d',
                error: colors.red.accent3,
            },
            dark: {
                primary: '#806582',
                secondary: '#181818',
                accent: '#899a7d',
                error: colors.red.accent3,
            },
        },
    },
});
