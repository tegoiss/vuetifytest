import firebase from 'firebase/app'
import 'firebase/firestore'

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
    apiKey: "AIzaSyBWyCEsIlvMcwCIV1MldiUVcjsm61ZrjhE",
    authDomain: "vuetifytest-1a89e.firebaseapp.com",
    databaseURL: "https://vuetifytest-1a89e.firebaseio.com",
    projectId: "vuetifytest-1a89e",
    storageBucket: "vuetifytest-1a89e.appspot.com",
    messagingSenderId: "890912638339",
    appId: "1:890912638339:web:54dee9dfbbc05b082862b6",
    measurementId: "G-BHYS60HGZ6"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
const db = firebase.firestore();

db.settings({ timestampsInSnapshots: true });

export default db;